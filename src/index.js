/**
 * implementation of the assignment 03 - cloudflare interview
 */

import { Router } from 'itty-router';
// access the token through environment variable
import config from './secrets';
import builder from './requestbuilder';


// _rt - the router instance
//const _rt = Router();
const _rt = Router();
const _client = builder.init(config.token);
// the target domain for the assignment
const _domain = "bnerdy.icu";

// default api
_rt.get('/', (request, event) => {
	return new Response(`
		<h1>welcome to the assignment 03 - cloudflare workers</h1>
		<p/>
		<a href='/dnsrecords'>get dns records</a>
		<br>
		<a href='/geo'>get geo meta data</a>
	`, { headers: { "Content-Type": "text/html", } })
});

// [/dnsrecords] return all DNS records based on the zone named "bnerdy.icu"
_rt.get('/dnsrecords', async (request, _) => {
	// get all the zone(s) first
	const _zones = await _client.Zones();
	let _zoneID = '';
	
	// filter by zone-name
	if (true == _zones.success) {
		_zones.result.forEach(function(_zone) {
			if (_domain == _zone.name) {
				_zoneID = _zone.id;
			}
		});
	}
	// if _zoneID is valid...
	if ('' == _zoneID) {
		return new Response(``, { status: 500, statusText: `no Zone matched the '${_domain}'`, });
	}
	// get the dns records
	const _dnsRecords = await _client.DNSRecords(_zoneID);
	if (true == _dnsRecords.success) {
		return new Response(JSON.stringify(_dnsRecords.result, null, 3));
	} else {
		return new Response(`failed to query for the zone's [${_domain}] DNS, error: ${_dnsRecords.errors}`);
	}
});

// [/geo] return client-ip, country and asn of the request.
_rt.get('/geo', function(request, _) {

	// [debug] - list out all header key-values (request IP is available under x-real-ip OR cf-connecting-ip header)
	/*
	let _keys = request.headers.keys();
	let _result = _keys.next();
	while (!_result.done) {
		console.log(`${_result.value} => ${request.headers.get(_result.value)}`);
		_result = _keys.next();
	}*/
	const _country = request.cf.country;
	const _ip = request.headers.get('CF-Connecting-IP');
	const _asn = request.cf.asn;

	// redirect to 1.1.1. if request not origin from SG
	if ('SG' != _country) {
		return Response.redirect('https://1.1.1.1');
	}
	return new Response(`<ul>
		<li><b>client-ip</b>: ${_ip} </li>
		<li><b>country</b>: ${_country} </li>
		<li><b>asn</b>: ${_asn} </li></ul>`, 
	{
		headers: {
			"Content-Type": "text/html",
		}
	});
});

// default route - all non defined routes would result in 404
_rt.all("*", () => new Response("404, not found!", { status: 404 }));

// a generic error handler
const errHandler = error => new Response(error.message || 'Server Error', { status: error.status || 500 });

// attach the router "handle" to the event handler
addEventListener('fetch', event =>
  event.respondWith(_rt.handle(event.request, event).catch(errHandler))
);
